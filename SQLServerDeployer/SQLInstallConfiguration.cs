﻿namespace SQLServerDeployer
{
    public class SQLInstallConfiguration
    {
        public string addLocal { get; set; }
        public string SqlInstallDirectory { get; set; }
        public bool ReportErrors { get; set; }
        public bool DisableNetworkProtocols { get; set; }
        public string Collation { get; set; }
        public string SysadminPassword { get; set; }
        public bool UseSQLSecurityMode { get; set; }
        public string SqlServicePassword { get; set; }
        public string SqlServiceAccountName { get; set; }
        public string SqlBrowserPassword{ get; set; }
        public string SqlBrowserAccountName { get; set; }
        public bool AutostartSQLBrowserService { get; set; }
        public bool AutostartSQLService{ get; set; }
        public string SqlDataDirectory{ get; set; }
        public string SqlInstallSharedDirectory { get; set; }
        public string SetupFileLocation { get; set; }
        public string InstanceName { get; set; }
    }
}
