﻿using System;
using System.Linq;
using System.ServiceProcess;
using System.Threading;

namespace SQLServerDeployer
{
    public class ServiceSwitch
    {
        #region public void StopAllServices(bool showErrorMessage)
        /// <summary>
        /// Stops the SQL Services.
        /// </summary>
        /// <param name="showErrorMessage"></param>
        public void StopAllServices(bool showErrorMessage)
        {
            ServiceController[] scServicesToStop = ServiceController.GetServices();
            
            int counter;
            counter = 0;

            foreach (ServiceController scTemp in scServicesToStop)
            {
                switch (scTemp.Status)
                {
                    case ServiceControllerStatus.Running:
                        try
                        {
                            scTemp.Stop();
                            counter++;
                        }
                        catch (Exception exc)
                        {
                            if (showErrorMessage)
                            {
                                throw new Exception("Error in Class ServiceSwitch.StopAllSQLServices " + exc.Message);
                            }
                        }
                        break;
                }

            }

        }
        #endregion

        #region public void StopAllSQLServices(bool showErrorMessage)
        /// <summary>
        /// Stops the SQL Services.
        /// </summary>
        /// <param name="showErrorMessage"></param>
        public void StopAllSQLServices(bool showErrorMessage)
        {
            //string[] sqlServices = {"MsDtsServer100", "MSSQL$VOVCHUK", "MSSQLFDLauncher", "MSSQLSERVER",
            //"MSSQLServerOLAPService", "SQLBrowser", "SQLWriter"};
            ServiceController[] scServicesToStop = ServiceController.GetServices();


            int counter = 0;

            foreach (ServiceController scTemp in scServicesToStop)
            {

                scTemp.MachineName = @"LOCALHOST";

                if (scTemp.Status == ServiceControllerStatus.Running)
                {
                    if ((scTemp.ServiceName == "MsDtsServer100") || (scTemp.ServiceName == "MSSQL$VOVCHUK") || (scTemp.ServiceName == "MSSQLSERVER") ||
                        (scTemp.ServiceName == "MSSQLFDLauncher") || (scTemp.ServiceName == "MSSQLServerOLAPService") ||
                        (scTemp.ServiceName == "SQLBrowser") || (scTemp.ServiceName == "SQLWriter"))
                    {
                        try
                        {
                            scTemp.Stop();
                            counter++;
                        }
                        catch (Exception exc)
                        {
                            if (showErrorMessage)
                            {
                                throw new Exception("Error in Class ServiceSwitch.StopAllSQLServices " + exc.Message);
                            }

                        }

                    }

                }

           } 

        }
        #endregion

        #region public void StopOnlySQLMainServices(bool showErrorMessage)
        /// <summary>
        /// Stops the SQL Services.
        /// </summary>
        /// <param name="showErrorMessage"></param>
        public void StopOnlySQLMainServices(bool showErrorMessage)
        {
            //string[] sqlServices = {"MsDtsServer100", "MSSQL$VOVCHUK", "MSSQLFDLauncher", "MSSQLSERVER",
            //"MSSQLServerOLAPService", "SQLBrowser", "SQLWriter"};
            ServiceController[] scServicesToStop = ServiceController.GetServices();
            int counter = 0;
            foreach (ServiceController scTemp in scServicesToStop)
            {
                if (scTemp.Status == ServiceControllerStatus.Running)
                {
                    if ((scTemp.ServiceName == "MSSQL$VOVCHUK") || (scTemp.ServiceName == "MSSQLSERVER"))
                    {
                        try
                        {
                            scTemp.Stop();
                            counter++;
                        }
                        catch (Exception exc)
                        {
                            if (showErrorMessage)
                            {
                                throw new Exception("Error in Class ServiceSwitch.StopAllSQLServices " + exc.Message);
                            }

                        }

                    }

                }

            }

        }
        #endregion

        #region public void StartAllSQLServices(bool showErrorMessage)
        /// <summary>
        /// Starts all Sql Services.
        /// </summary>
        /// <param name="showErrorMessage"></param>
        public void StartAllSQLServices(bool showErrorMessage)
        {
            ServiceController[] scServicesToStart = ServiceController.GetServices();

            int counter = 0;

            foreach (ServiceController scTemp in scServicesToStart)
            {
                switch (scTemp.Status)
                {
                    case ServiceControllerStatus.Stopped:
                        if ((scTemp.ServiceName == "MsDtsServer100") || (scTemp.ServiceName == "MSSQL$VOVCHUK") || (scTemp.ServiceName == "MSSQLSERVER") ||
                            (scTemp.ServiceName == "MSSQLFDLauncher") || (scTemp.ServiceName == "MSSQLServerOLAPService") ||
                            (scTemp.ServiceName == "SQLBrowser") || (scTemp.ServiceName == "SQLWriter"))
                        {
                            try
                            {
                                scTemp.Start();
                                counter++;
                            }
                            catch (Exception exc)
                            {
                                if (showErrorMessage)
                                {
                                    throw new Exception("Error in Class ServiceSwitch.StartAllSQLServices: " +
                                                        exc.Message);
                                }

                            }

                        }
                        break;
                }

            }
        }
        #endregion

        #region public void PauseAllSQLServices(bool showErrorMessage)
        /// <summary>
        /// Starts all Sql Services.
        /// </summary>
        /// <param name="showErrorMessage"></param>
        public void PauseAllSQLServices(bool showErrorMessage)
        {
            ServiceController[] scServicesToStart = ServiceController.GetServices();

            int counter = 0;

            foreach (ServiceController scTemp in
                scServicesToStart.Where(scTemp => scTemp.Status == ServiceControllerStatus.Running).Where(scTemp => (scTemp.ServiceName == "MsDtsServer100") || (scTemp.ServiceName == "MSSQL$VOVCHUK") || (scTemp.ServiceName == "MSSQLSERVER") || (scTemp.ServiceName == "MSSQLFDLauncher") || (scTemp.ServiceName == "MSSQLServerOLAPService") || (scTemp.ServiceName == "SQLBrowser") || (scTemp.ServiceName == "SQLWriter")))
            {
                try
                {
                    if (scTemp.CanPauseAndContinue)
                    {
                        scTemp.Pause();
                        counter++;
                    }
                }
                catch (Exception exc)
                {
                    if (showErrorMessage)
                    {
                        throw new Exception("Error in Class ServiceSwitch.StartAllSQLServices: " +
                                            exc.Message);
                    }

                }
            }
        }
        #endregion

        #region public void RestartPausedSQLServices(bool showErrorMessage)
        /// <summary>
        /// Starts all Sql Services.
        /// </summary>
        /// <param name="showErrorMessage"></param>
        public void RestartPausedSQLServices(bool showErrorMessage)
        {
            ServiceController[] scServicesToStart = ServiceController.GetServices();

            int counter = 0;

            foreach (ServiceController scTemp in scServicesToStart)
            {
                switch (scTemp.Status)
                {
                    case ServiceControllerStatus.Paused:
                        if ((scTemp.ServiceName == "MsDtsServer100") || (scTemp.ServiceName == "MSSQL$VOVCHUK") ||
                            (scTemp.ServiceName == "MSSQLSERVER") ||
                            (scTemp.ServiceName == "MSSQLFDLauncher") || (scTemp.ServiceName == "MSSQLServerOLAPService") ||
                            (scTemp.ServiceName == "SQLBrowser") || (scTemp.ServiceName == "SQLWriter"))
                        {
                            try
                            {
                                scTemp.Continue();
                                counter++;
                            }
                            catch (Exception exc)
                            {
                                if (showErrorMessage)
                                {
                                    throw new Exception("Error in Class ServiceSwitch.StartAllSQLServices: " +
                                                        exc.Message);
                                }
                            }
                        }
                        break;
                }
            }
        }
        #endregion

        #region public bool StopAndStartSQLServices(bool showMessages)
        /// <summary>
        /// Stops and starts service. Returns true when finished.
        /// </summary>
        /// <param name="showMessages"></param>
        /// <returns>boolean true when finished</returns>
        public bool StopAndStartSQLServices(bool showMessages)
        {
            bool done = false;

            try
            {
                StopAllSQLServices(showMessages);
                StartAllSQLServices(showMessages);
                done = true;
            }
            catch (Exception exc)
            {
                if (showMessages)
                {
                    throw new Exception("ServiceSwitch.StopAndStartSQLServices: " +
                                        exc.Message);
                }
            }
            return done;
        }
        #endregion

        public string CurrentRunningServices
        {
            get
            {
                ServiceController[] scServicesToRead = ServiceController.GetServices();

                return scServicesToRead.Where(scTemp => scTemp.Status == ServiceControllerStatus.Running).Aggregate("", (current, scTemp) => current + (scTemp.ServiceName + ", "));
            }
        }

        public string StoppedServices
        {
            get
            {
                ServiceController[] scServicesToRead = ServiceController.GetServices();

                return scServicesToRead.Where(scTemp => scTemp.Status == ServiceControllerStatus.Stopped).Aggregate("", (current, scTemp) => current + (scTemp.ServiceName + ", "));
            }
        }

        public string PausedServices
        {
            get
            {
                ServiceController[] scServicesToRead = ServiceController.GetServices();

                return scServicesToRead.Where(scTemp => scTemp.Status == ServiceControllerStatus.Paused).Aggregate("", (current, scTemp) => current + (scTemp.ServiceName + ", "));
            }
        }


        /// <summary>
        /// Propiedad que determina si el servicio de SQL esta detenido
        /// </summary>
        /// <returns></returns>
        public bool SQLServerIsStopped()
        {


            bool stopped = false;

            ServiceController[] scServicesToRead = ServiceController.GetServices();

            foreach (ServiceController scTemp in scServicesToRead)
            {
                switch (scTemp.ServiceName)
                {
                    case "MSSQLSERVER":
                    case "MSSQL$VOVCHUK":
                        switch (scTemp.Status)
                        {
                            case ServiceControllerStatus.Stopped:
                                stopped = true;
                                break;
                            case ServiceControllerStatus.StopPending:
                                Thread.Sleep(60000);
                                stopped = true;
                                break;
                            default:
                                stopped = false;
                                break;
                        }
                        break;
                }
            }
            return stopped;

        }

        public bool VOVCHUKServerIsStopped()
        {
            bool stopped = false;

            ServiceController[] scServicesToRead = ServiceController.GetServices();

            foreach (ServiceController scTemp in scServicesToRead)
            {
                switch (scTemp.ServiceName)
                {
                    case "MSSQL$VOVCHUK":
                        switch (scTemp.Status)
                        {
                            case ServiceControllerStatus.Stopped:
                                stopped = true;
                                break;
                            default:
                                stopped = false;
                                break;
                        }
                        break;
                }
            }
            return stopped;

        }

        #region public void StartVOVCHUKAgentsIfStopped(bool showErrorMessage)
        /// <summary>
        /// Starts all Sql Services.
        /// </summary>
        /// <param name="showErrorMessage"></param>
        public void StartVOVCHUKAgentsIfStopped(bool showErrorMessage)
        {
            ServiceController[] scServicesToStart = ServiceController.GetServices();

#pragma warning disable 219
            int counter = 0;
#pragma warning restore 219

            foreach (ServiceController scTemp in
                scServicesToStart.Where(scTemp => scTemp.Status == ServiceControllerStatus.Stopped).Where(scTemp => scTemp.ServiceName == "MSSQL$VOVCHUK"))
            {
                try
                {
                    scTemp.Start();

                    counter++;
                }
                catch (Exception exc)
                {
                    if (showErrorMessage)
                    {
                        throw new Exception("Error in Class ServiceSwitch.StartAllSQLServices: " +
                                            exc.Message);
                    }
                }
            }
        }
        #endregion

        #region public void StartAllVOVCHUKServiceIfStopped(bool showErrorMessage)
        /// <summary>
        /// Starts all Sql Services.
        /// </summary>
        /// <param name="showErrorMessage"></param>
        public void StartVOVCHUKServiceIfStopped(bool showErrorMessage)
        {
            ServiceController[] scServicesToStart = ServiceController.GetServices();

#pragma warning disable 219
            int counter = 0;
#pragma warning restore 219

            foreach (ServiceController scTemp in scServicesToStart)
            {
                switch (scTemp.Status)
                {
                    case ServiceControllerStatus.Stopped:
                        if ((scTemp.ServiceName == "MsDtsServer100") || (scTemp.ServiceName == "MSSQL$VOVCHUK") || (scTemp.ServiceName == "MSSQLSERVER") ||
                            (scTemp.ServiceName == "MSSQLFDLauncher") || (scTemp.ServiceName == "MSSQLServerOLAPService") ||
                            (scTemp.ServiceName == "SQLBrowser") || (scTemp.ServiceName == "SQLWriter"))
                        {
                            try
                            {
                                scTemp.Start();

                                counter++;
                            }
                            catch (Exception exc)
                            {
                                if (showErrorMessage)
                                {
                                    throw new Exception("Error in Class ServiceSwitch.StartAllSQLServices: " +
                                                        exc.Message);
                                }
                            }
                        }
                        break;
                }
            }
        }
        #endregion


    }
}