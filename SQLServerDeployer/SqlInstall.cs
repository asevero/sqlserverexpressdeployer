﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using Digitalports.XMLSerializator;
using System.Reflection;
using System.Globalization;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SQLServerDeployer
{
    [Serializable]
    public class SqlInstall
    {
        #region INTERNAL VARIABLES

        //Variables for setup.exe command line




        #endregion

        #region CONSTRUCTORES

        public SqlInstall()
        {
            AddLocal = "all";

            SqlInstallDirectory = "";
            ReportErrors = true;
            DisableNetworkProtocols = true;
            Collation = "";
            SysadminPassword = "vovchuk";
            UseSqlSecurityMode = false;
            SqlServicePassword = "";
            SqlServiceAccountName = "";
            SqlBrowserPassword = "";
            SqlBrowserAccountName = "";
            AutostartSqlBrowserService = false;
            AutostartSqlService = true;
            SqlDataDirectory = "";
            SqlInstallSharedDirectory = "";
            SetupFileLocation = @"C:\VovchukCatalogo\installers\SQLEXPR.EXE";

            InstanceName = "VOVCHUK";
        }

        public SqlInstall(string filenameString)
        {
            SQLInstallConfiguration sqlInstallInstance = null;

            if (File.Exists(filenameString))
            {
                sqlInstallInstance =
                    ObjectXMLSerializer<SQLInstallConfiguration>.Load(filenameString);
            }
            else
            {
                MessageBox.Show("Archivo de configuracion no cargado o carga incorrecta");
            }



            AddLocal = "All";

            // si no viene vacia, creo los datos para llenarlos
            if (sqlInstallInstance == null) return;

            SqlInstallDirectory = sqlInstallInstance.SqlInstallDirectory;

            ReportErrors = sqlInstallInstance.ReportErrors;

            DisableNetworkProtocols = sqlInstallInstance.DisableNetworkProtocols;

            Collation = sqlInstallInstance.Collation;

            SysadminPassword = sqlInstallInstance.SysadminPassword;

            UseSqlSecurityMode = sqlInstallInstance.UseSQLSecurityMode;

            SqlServicePassword = sqlInstallInstance.SqlServicePassword;

            SqlServiceAccountName = sqlInstallInstance.SqlServiceAccountName;

            SqlBrowserPassword = sqlInstallInstance.SqlBrowserPassword;

            SqlBrowserAccountName = sqlInstallInstance.SqlBrowserAccountName;

            AutostartSqlBrowserService = sqlInstallInstance.AutostartSQLBrowserService;

            AutostartSqlService = sqlInstallInstance.AutostartSQLService;

            SqlDataDirectory = sqlInstallInstance.SqlDataDirectory;

            SqlInstallSharedDirectory = sqlInstallInstance.SqlInstallSharedDirectory;

            SetupFileLocation = sqlInstallInstance.SetupFileLocation;

            InstanceName = sqlInstallInstance.InstanceName;


        }

        #endregion

        #region PROPIEDADES

        public string AddLocal { get; set; }

        public string InstanceName { get; set; }

        public string SetupFileLocation { get; set; }

        public string SqlInstallSharedDirectory { get; set; }

        public string SqlDataDirectory { get; set; }

        public bool AutostartSqlService { get; set; }

        public bool AutostartSqlBrowserService { get; set; }

        public string SqlBrowserAccountName { get; set; }

        public string SqlBrowserPassword { get; set; }

        //Defaults to LocalSystem
        public string SqlServiceAccountName { get; set; }

        public string SqlServicePassword { get; set; }

        public bool UseSqlSecurityMode { get; set; }

        public string SysadminPassword { private get; set; }

        public string Collation { get; set; }

        public bool DisableNetworkProtocols { get; set; }

        public bool ReportErrors { get; set; }

        public string SqlInstallDirectory { get; set; }

        #endregion

        #region METODOS

        public static bool IsExpressInstalled()
        {
            var arquitecturaProcesador = GetOsArquitechture();


            if (arquitecturaProcesador == ProcessorArchitecture.Amd64 || arquitecturaProcesador == ProcessorArchitecture.IA64)
            {

                var key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);


                var rk = key.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server");

                if (rk != null)
                {

                    var instances = (String[])rk.GetValue("InstalledInstances");

                    {
                        if (instances == null) return false;

                        List<String> instanceNameList = instances.ToList();

                        var ob = from p in instanceNameList
                                 where String.CompareOrdinal(p, "VOVCHUK") == 0
                                 select p;

                        key.Dispose();
                        
                        return ob.Count() == 1;
                    }
                }

                key.Dispose();
                return false;
            }


            else
            {
                var key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default);


                var rk = key.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server");

                if (rk != null)
                {

                    var instances = (String[])rk.GetValue("InstalledInstances");

                    {
                        if (instances == null) return false;

                        List<String> instanceNameList = instances.ToList();

                        var ob = from p in instanceNameList
                                 where String.CompareOrdinal(p, "VOVCHUK") == 0
                                 select p;

                        return ob.Count() == 1;
                    }
                }

                return false;
            }
        }

        public static int EnumSqlInstances(ref string[] strInstanceArray, ref string[] strEditionArray, ref string[] strVersionArray)
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Microsoft SQL Server\\", false))
            {
                if (key == null) return 0;

                string[] strNames = key.GetSubKeyNames();

                //If we can not find a SQL Server registry key, we return 0 for none
                if (strNames.Length == 0) return 0;

                //How many instances do we have?

                int iNumberOfInstances = strNames.Count(s => s.StartsWith("MSSQL."));

                //Reallocate the string arrays to the new number of instances
                strInstanceArray = new string[iNumberOfInstances];
                strVersionArray = new string[iNumberOfInstances];
                strEditionArray = new string[iNumberOfInstances];
                int iCounter = 0;

                foreach (string s in strNames)
                {
                    if (!s.StartsWith("MSSQL."))
                        continue;

                    //Get Instance name
                    using (RegistryKey keyInstanceName = key.OpenSubKey(s, false))
                    {
                        if (keyInstanceName != null) strInstanceArray[iCounter] = (string)keyInstanceName.GetValue("");
                    }

                    //Get Edition
                    using (var keySetup = key.OpenSubKey(s + "\\Setup\\", false))
                    {
                        if (keySetup != null)
                        {
                            strEditionArray[iCounter] = (string)keySetup.GetValue("Edition");
                            strVersionArray[iCounter] = (string)keySetup.GetValue("Version");
                        }
                    }

                    iCounter++;
                }
                return iCounter;
            }
        }

// ReSharper disable once UnusedMethodReturnValue.Local
        private string BuildCommandLine()
        {
            var strCommandLine = new StringBuilder();

            if (!string.IsNullOrEmpty(SqlInstallDirectory))
            {
                strCommandLine.Append("INSTALLSQLDIR=\"").Append(SqlInstallDirectory).Append("\"");
            }

            if (!string.IsNullOrEmpty(SqlInstallSharedDirectory))
            {
                strCommandLine.Append(" INSTALLSQLSHAREDDIR=\"").Append(SqlInstallSharedDirectory).Append("\"");
            }

            if (!string.IsNullOrEmpty(SqlDataDirectory))
            {
                strCommandLine.Append(" INSTALLSQLDATADIR=\"").Append(SqlDataDirectory).Append("\"");
            }

            if (!string.IsNullOrEmpty(AddLocal))
            {
                strCommandLine.Append(" ADDLOCAL=\"").Append(AddLocal).Append("\"");
            }

            strCommandLine.Append(AutostartSqlService ? " SQLAUTOSTART=1" : " SQLAUTOSTART=0");

            strCommandLine.Append(AutostartSqlBrowserService ? " SQLBROWSERAUTOSTART=1" : " SQLBROWSERAUTOSTART=0");

            if (!string.IsNullOrEmpty(SqlBrowserAccountName))
            {
                strCommandLine.Append(" SQLBROWSERACCOUNT=\"").Append(SqlBrowserAccountName).Append("\"");
            }

            if (!string.IsNullOrEmpty(SqlBrowserPassword))
            {
                strCommandLine.Append(" SQLBROWSERPASSWORD=\"").Append(SqlBrowserPassword).Append("\"");
            }

            if (!string.IsNullOrEmpty(SqlServiceAccountName))
            {
                strCommandLine.Append(" SQLACCOUNT=\"").Append(SqlServiceAccountName).Append("\"");
            }

            if (!string.IsNullOrEmpty(SqlServicePassword))
            {
                strCommandLine.Append(" SQLPASSWORD=\"").Append(SqlServicePassword).Append("\"");
            }

            if (UseSqlSecurityMode)
            {
                strCommandLine.Append(" SECURITYMODE=SQL");
            }

            if (!string.IsNullOrEmpty(SysadminPassword))
            {
                strCommandLine.Append(" SAPWD=\"").Append(SysadminPassword).Append("\"");
            }
            else
            {
                strCommandLine.Append(String.Format(" SAPWD={0} ", SysadminPassword));
            }

            if (!string.IsNullOrEmpty(Collation))
            {
                strCommandLine.Append(" SQLCOLLATION=\"").Append(Collation).Append("\"");
            }

            strCommandLine.Append(DisableNetworkProtocols
                                      ? " DISABLENETWORKPROTOCOLS=1"
                                      : " DISABLENETWORKPROTOCOLS=0");

            strCommandLine.Append(ReportErrors ? " ERRORREPORTING=1" : " ERRORREPORTING=0");

            return strCommandLine.ToString();
        }

        public bool InstallExpress()
        {

            BuildCommandLine();

            // Argumentos POST-CODE

            ProcessorArchitecture arquitecturaProcesador = GetOsArquitechture();

            if (arquitecturaProcesador == ProcessorArchitecture.None)
            {
                MessageBox.Show("Error, no se pudo determinar la arquitectura de su procesador, el sistema es solo compatible con Procesadores Intel, AMD 64 y 32  Bits", "Error al detectar Arquitectura de Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                Application.Exit();
            }



            string fileInnerText = String.Empty;

            CultureInfo ci = CultureInfo.InstalledUICulture;
            switch (ci.TwoLetterISOLanguageName)
            {

                case "es":
                    {

                        if (arquitecturaProcesador == ProcessorArchitecture.Amd64 || arquitecturaProcesador == ProcessorArchitecture.IA64) // 64 Bits
                        {
                            fileInnerText = @"SpanishSetup64.bat";
                        }
                        else
                        {
                            fileInnerText = @"SpanishSetup32.bat";
                        }
                    }
                    break;

                case "en":
                    {
                        if (arquitecturaProcesador == ProcessorArchitecture.Amd64 || arquitecturaProcesador == ProcessorArchitecture.IA64) // 64 Bits
                        {
                            fileInnerText = @"EnglishSetup64.bat";
                        }
                        else
                        {
                            fileInnerText = @"EnglishSetup32.bat";
                        }
                    }
                    break;
            }

            //ubicacion = @"C:\VovchukCatalogo\installers\SQLExpr_x64_enu.exe";
            string ubicacion = String.Format(@"C:\VovchukCatalogo\installers\{0}", fileInnerText);


            //string temporal = @"/Q  /ConfigurationFile=C:\VovchukCatalogo\installers\ConfigurationFile_modificado.INI";

            //In both cases, we run Setup because we have the file.

            using (var myProcess = new Process { StartInfo = { FileName = ubicacion, CreateNoWindow = true, UseShellExecute = false, WindowStyle = ProcessWindowStyle.Hidden } })
            {
                bool processOk = myProcess.Start();
                myProcess.WaitForExit();
                return processOk;
            }


        }

        /// <summary>
        /// Eval if the database exist on the specified server
        /// </summary>
        /// <param name="databasename">String name of the database to find</param>
        /// <param name="servername">String that provides the server name wheres i want to lookup</param>
        /// <param name="instance">if the database exist inside a server with an named instance, pass the name of the instance, if not, specify null</param>
        /// <returns>Returns true if the database exist, else return false</returns>
        public static bool DataBaseExits(string databasename, string servername, string instance)
        {
            var dt = new DataTable();

            using (var con = new SqlConnection())
            {

                con.ConnectionString = "Data Source=LOCALHOST\\VOVCHUK;Initial Catalog=master;Persist Security Info=True;User ID=sa;Password=vovchuk";

                var sqlCommand = new SqlCommand("sp_databases", con) { CommandType = CommandType.StoredProcedure };

                con.Open();

                var dr = sqlCommand.ExecuteReader();

                dt.Load(dr);

                con.Close();
            }


            var objList = dt.AsEnumerable().Where(d => String.CompareOrdinal(d[0].ToString(), "wcClient") == 0);

            List<DataRow> listaDBs = objList.ToList();

            bool databaseExist = listaDBs.Count != 0;



            // retorno el valor de la evaluacion
            return databaseExist;
        }

        /// <summary>
        /// Saves the configuration inside a XML file
        /// </summary>
        /// <param name="pathFilenameToSave">Parameter to provide the path of the filename where you want save the config file</param>
        /// <param name="configInstance">Instance of class where have the params to save the configuration</param>
        public static void SaveFileConfiguration(string pathFilenameToSave, SQLInstallConfiguration configInstance)
        {
            ObjectXMLSerializer<SQLInstallConfiguration>.Save(configInstance, pathFilenameToSave);
        }

        public static ProcessorArchitecture GetOsArquitechture()
        {

            var arquitectura = new ProcessorArchitecture();

            using (var keyMain = Registry.LocalMachine.OpenSubKey(@"HARDWARE\DESCRIPTION\System\CentralProcessor\0"))
            {
                if (keyMain != null)
                {
                    var objeto = keyMain.GetValue("Identifier").ToString().Split(' ');
                    switch (objeto[0].Trim())
                    {
                        case "Intel64":
                            {
                                arquitectura = ProcessorArchitecture.IA64;
                            }
                            break;
                        case "x86":
                            {
                                arquitectura = ProcessorArchitecture.X86;
                            }
                            break;
                        case "AMD64":
                            {
                                arquitectura = ProcessorArchitecture.Amd64;
                            }
                            break;
                    }
                }
            }

            return arquitectura;

        }

        #endregion
    }
}
