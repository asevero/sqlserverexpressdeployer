﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using SQLServerDeployer;

namespace TestSQL
{
    public partial class Form1 : Form
    {
        private SqlConnection sqlCon = new SqlConnection();

        private SqlCommand sqlCmd = new SqlCommand();

        public Form1()
        {
            InitializeComponent();

            if (SetupDatabase() == false)
            {
                return;
            }

            PopulateGrid();
        }

        public bool SetupDatabase()
        {
            bool bContinue = false;

            //Create a connection to SQL Server
            try
            {
                sqlCon.ConnectionString = "Server=.\\sqlexpress;Integrated Security=true";
                sqlCon.Open();
            }
            catch (SqlException sql_ex)
            {
                MessageBox.Show(String.Format(@"Fail to connect to SQL Server Express{0} {1}", sql_ex.Number, sql_ex.Message));

                return false;
            }

            //Now that you are connected to Express, check the database versions

            switch (CheckVersion())
            {
                case (int)VersionCheck.Equal:
                    {
                        bContinue = true;
                        break;
                    }
                case (int)VersionCheck.Failed:
                    {
                        break;
                    }
                case (int)VersionCheck.DatabaseIsOlder:
                    {
                        //Run the upgrade script
                        //bContinue = RunScript(Resource1.UpdateAdventureWorks.ToString());
                        break;
                    }
                case (int)VersionCheck.DatabaseIsMoreNew:
                    {
                        break;
                    }
                case (int)VersionCheck.DatabaseNotFound:
                    {
                        //Run the creation script
                        //bContinue = RunScript(Resource1.CreateAdventureWorks.ToString());
                        break;
                    }
                default:
                    {
                        break;
                    }

            }


            return bContinue;


        }

        public bool RunScript(string strFile)
        {
            string[] strCommands = ParseScriptToCommands(strFile);

            try
            {
                if (sqlCon.State != ConnectionState.Open) sqlCon.Open();

                sqlCmd.Connection = sqlCon;

                foreach (string strCmd in strCommands)
                {
                    if (strCmd.Length > 0)
                    {
                        sqlCmd.CommandText = strCmd;
                        sqlCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException sql_ex)
            {
                MessageBox.Show(sql_ex.Number + @" " + sql_ex.Message);
                return false;
            }

            return true;
        }

        public int CheckVersion()
        {
            //Get Version information from application

            Version v = new Version(Application.ProductVersion);

            try
            {
                //Verify that the AdventureWorks Database exists
                sqlCmd = new SqlCommand("SELECT COUNT(*) FROM master..sysdatabases WHERE name='AdventureWorks'", sqlCon);

                string strResult = sqlCmd.ExecuteScalar().ToString();

                if (strResult == "0")
                {
                    sqlCon.Close();
                    return (int)VersionCheck.DatabaseNotFound;
                }

                sqlCmd = new SqlCommand("SELECT value FROM AdventureWorks..AppInfo WHERE property='version'", sqlCon);

                strResult = (string)sqlCmd.ExecuteScalar();

                Version vDb = new Version(strResult);

                sqlCon.Close();

                if (vDb == v)
                    return (int)VersionCheck.Equal;

                if (vDb > v)
                    return (int)VersionCheck.DatabaseIsMoreNew;

                if (vDb < v)
                    return (int)VersionCheck.DatabaseIsOlder;

            }
            catch (SqlException sql_ex)
            {
                MessageBox.Show(sql_ex.Number + @" " + sql_ex.Message);
                return (int)VersionCheck.Failed;
            }
            catch (Exception system_ex)
            {
                MessageBox.Show(system_ex.Message);
                return (int)VersionCheck.Failed;
            }

            return (int)VersionCheck.Failed;

        }

        public string[] ParseScriptToCommands(string strScript)
        {
            string[] commands = Regex.Split(strScript, "GO\r\n", RegexOptions.IgnoreCase);

            return commands;
        }

        public void PopulateGrid()
        {

            const string strCmd = "Select * from [AdventureWorks].[HumanResources].[Department]";

            SqlDataAdapter da = new SqlDataAdapter(strCmd, sqlCon);

            DataSet ds = new DataSet();

            da.Fill(ds, "Departments");

            //dataGridView1.DataSource = ds;

            //dataGridView1.DataMember = "Departments";

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'adventureWorks_DataDataSet.Department' table. You can move or remove this line as necessary.

            //this.departmentTableAdapter.Fill(this.adventureWorks_DataDataSet.Department);
        }

        public enum VersionCheck { Failed = 0, Equal, DatabaseIsMoreNew, DatabaseIsOlder, DatabaseNotFound };

        private void button1_Click(object sender, EventArgs e)
        {
            SQLInstallConfiguration install = new SQLInstallConfiguration
                                                  {
                                                      SqlDataDirectory = @"C:\DB\",
                                                      AutostartSQLService = true,
                                                      InstanceName = "SQLEXPRESS",
                                                      addLocal = "All",
                                                      Collation = "",
                                                      AutostartSQLBrowserService = true,
                                                      SetupFileLocation = @"C:\VovchukCatalogo\SQLEXPR.EXE"
                                                  };


            //if (install.IsExpressInstalled())
            //{
            //    listBox1.Items.Add("HAY UNA VERSION INSTALADA");
            //}
            //else
            //{
            //    listBox1.Items.Add("NO ENCONTRE VERSION DE SQL INSTALADA");
            //}

            //bool instalacionExitosa = install.InstallExpress();

            SQLInstall.SaveFileConfiguration(@"C:\db\config.xml", install);


            SQLInstall loadinstance = new SQLInstall(@"C:\db\config.xml");


            loadinstance.InstallExpress();
        }
    }

}
